-- Lua Make Version: 1.0.1
-- Works well on Linux, on windows, the OutputCommand config can cause problems if disabled.

local LMake = {}

-- Config:
LMake.Config = {
	ExitOnFailture = true,
	EchoCommand    = true,
	OutputCommand  = true,
	EchoTarget     = true
}

-- Target table:
LMake.Targets = {}
LMake.TargetV = {
	MasterTarget  = "master",
	CurrentTarget = false
}

-- Engine:
function LMake.RaiseError(msg, code)
	if LMake.TargetV.CurrentTarget then
		msg = "Raised error on target '" .. LMake.TargetV.CurrentTarget .. "': " .. msg
	end
	print(msg)
	os.exit(code)
end

function LMake.Shell(command)
	if LMake.Config.EchoCommand then
		print(command)
	end
	if not LMake.Config.OutputCommand then
		command = command .. " > /dev/null"
	end
	Status = os.execute(command)
	Status = Status / 256
	if LMake.Config.ExitOnFailture then
		if Status ~= 0 then
			LMake.RaiseError("Command execution failed: " .. command, Status)
		end
	else
		return Status
	end
end

function LMake.Target(name, func, dependencies)
	LMake.Targets[name] = {Depends = dependencies, Function = func}
end

function LMake.DefaultTarget(target)
	if LMake.Targets[target] then
		LMake.TargetV.MasterTarget = target
	else
		LMake.RaiseError("Target '" .. target .. "' doesn't exists!", 1)
	end
end

function LMake.RunTarget(target)
	if LMake.Targets[target] then
		if LMake.Config.EchoTarget then
			print("Entered target: " .. target)
		end
		if LMake.Targets[target].Depends and type(LMake.Targets[target].Depends) == "table" then
			for T = 1, #LMake.Targets[target].Depends do
				LMake.RunTarget(LMake.Targets[target].Depends[T])
			end
		end
		LMake.TargetV.CurrentTarget = target
		LMake.Targets[target].Function()
		LMake.TargetV.CurrentTarget = false
	else
		LMake.RaiseError("Target '" .. target .. "' doesn't exists!", 1)
	end
end

function LMake.Make(args)
	if args and args[1] then
		LMake.SetDefaultTarget(args[1])
	end
	LMake.RunTarget(LMake.TargetV.MasterTarget)
end

-- Loader:
if LMakeNoGlobal then
	LMakeNoGlobal = nil
	return LMake
else
	for k, v in pairs(LMake) do
		_G[k] = v
	end
end
